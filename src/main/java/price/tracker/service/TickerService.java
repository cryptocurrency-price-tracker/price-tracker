package price.tracker.service;

import com.binance.connector.client.impl.SpotClientImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import price.tracker.model.BinanceSymbol;
import price.tracker.model.TickerStatistics;
import price.tracker.repository.TickerRepository;

import java.util.*;

@Service
@AllArgsConstructor
@Slf4j
@EnableScheduling
public class TickerService {

    private final SpotClientImpl binanceSpotClientImpl;
    private final TickerRepository repository;
    private final ObjectMapper objectMapper;

    @Scheduled(fixedRate = 60000)
    public void fetchCryptoPrices() throws JsonProcessingException {
        Map<String, Object> tickers = new HashMap<>();
        tickers.put("symbols", new ArrayList<>(Arrays.asList(BinanceSymbol.BTCUSDT.getSymbol(),
                BinanceSymbol.BNBUSDT.getSymbol())));
        String result = binanceSpotClientImpl.createMarket().ticker24H(tickers);
        List<TickerStatistics> tickerStatisticsList = objectMapper.readValue(result, objectMapper.getTypeFactory()
                .constructCollectionType(List.class, TickerStatistics.class));
        repository.saveAll(tickerStatisticsList);
    }
}
