package price.tracker.model;

public enum BinanceSymbol {
    BTCUSDT("BTCUSDT"),
    ETHUSDT("ETHUSDT"),
    XRPUSDT("XRPUSDT"),
    BNBUSDT("BNBUSDT"),
    LTCUSDT("LTCUSDT"),
    ADAUSDT("ADAUSDT"),
    DOTUSDT("DOTUSDT"),
    UNIUSDT("UNIUSDT"),
    DOGEUSDT("DOGEUSDT"),
    LINKUSDT("LINKUSDT");

    private final String symbol;

    BinanceSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
