package price.tracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import price.tracker.model.TickerStatistics;

public interface TickerRepository extends JpaRepository<TickerStatistics, Long> {
}
