package price.tracker.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import price.tracker.model.TickerStatistics;
import price.tracker.repository.TickerRepository;

import java.util.List;

@RestController
@AllArgsConstructor
public class CryptoPriceController {

    private final TickerRepository repository;

    @GetMapping("/prices")
    public List<TickerStatistics> getPrices() {
        return repository.findAll();
    }
}
